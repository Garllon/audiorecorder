## AudiRecorder ##

The AudiRecorder is a tool to analyze the input of the microphone and calculate
the decibel value. If the decibel value is to high the System is warning you.

https://gowalker.org/github.com/rossdylan/influxdbc#NewSeries

## InfluxDB

The offical version for influxdb
https://github.com/influxdb/influxdb/blob/master/client/examples/example.go

Not Supported any more, but interesting
http://influxdb.com/docs/v0.8/introduction/getting_started.html
