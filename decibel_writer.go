package main

import (
	"github.com/influxdb/influxdb/client"
	"net/http"
)

func createClient() (*client.Client, error) {
	config := &client.ClientConfig{
		Host:       "localhost:8086",
		Username:   "root",
		Password:   "root",
		Database:   "decibelData",
		HttpClient: http.DefaultClient,
	}

	c, err := client.NewClient(config)
	if err != nil {
		return nil, err
	} else {
		return c, nil
	}
}

func createEntry(clientSession *client.Client, seriesName string, columnName string, value float64) string {
	series := &client.Series{
		Name:    seriesName,
		Columns: []string{columnName},
		Points: [][]interface{}{
			{value},
		},
	}

	err := clientSession.WriteSeries([]*client.Series{series})
	if err != nil {
		panic(err)
	} else {
		return "OK"
	}
}
