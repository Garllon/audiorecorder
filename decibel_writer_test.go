package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateEntry(t *testing.T) {
	clientSession, _ := createClient()

	fmt.Printf("SESSION: %v", clientSession)

	assert.Equal(t, "OK", createEntry(clientSession, "Decibel", "Bennet", 25), "Returns OK, if the entry was create")
}
