package main

import (
	"code.google.com/p/portaudio-go/portaudio"
	"fmt"
	"math"
	"os"
	"os/signal"
)

func main() {
	fmt.Println("Recording.  Press Ctrl-C to stop.")

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, os.Kill)

	portaudio.Initialize()
	defer portaudio.Terminate()
	in := make([]int32, 64)
	stream, err := portaudio.OpenDefaultStream(1, 0, 44100, len(in), in)
	chk(err)
	defer stream.Close()

	chk(stream.Start())

	decibelDuration := make([]float64, 0)
	index := 0

	for {
		chk(stream.Read())
		decibelValue := CalcDecibel(in)

		clientSession, _ := createClient()

		if index%200 == 0 {
			createEntry(clientSession, "Decibel", "Hendrik", decibelValue)
			index = 0
			decibelDuration = make([]float64, 0)
		} else {
			decibelDuration = append(decibelDuration, decibelValue)
			index = index + 1
		}

		select {
		case <-sig:
			return
		default:
		}
	}
	chk(stream.Stop())
}

func CalcDecibelDuration(v []float64) float64 {
	return 0.0
}

func CalcDecibel(v []int32) float64 {
	var value float64 = 0
	for i := 0; i < len(v); i++ {
		if v[i] == 0 {
			value = value + 0
		} else {
			value = value + (20 * math.Log10(math.Abs(float64(v[i])/32768)))
		}
	}

	if value > 0 {
		return value / 64
	} else {
		return value
	}
}
